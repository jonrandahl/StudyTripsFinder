﻿using System;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using System.Collections.Generic;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using StudyTripsFinder.Forms.Models;
using StudyTripsFinder.Forms.Utility;
using hbehr.recaptcha;

namespace StudyTripsFinder.Forms.Controllers
{
  public class TripQuoteFormController : SurfaceController
  {
    private readonly MailHelper _mailHelper = new MailHelper();
    private const int FormFolderId = 1323;

    public ActionResult RenderTripQuoteForm(string tripName, string priceFrom, string duration, List<CheckboxItem> enhanceTripOptions)
    {
      return PartialView("~/Views/Partials/Forms/TripQuoteFormView.cshtml", new TripQuoteForm
      {
        TripName = tripName,
        BasePrice = priceFrom,
        EnhanceTripOptions = enhanceTripOptions
      });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult ProcessFormSubmission(TripQuoteForm model)
    {

      string userResponse = HttpContext.Request.Params["g-recaptcha-response"];
      bool validCaptcha = ReCaptcha.ValidateCaptcha(userResponse);

      if (!validCaptcha)
      {
        TempData["TripQuoteFormValidationFailed"] = "The form did not pass the Google ReCaptcha validation.";

        return CurrentUmbracoPage();
      }


      if (!ModelState.IsValid)
      {
        TempData["TripQuoteFormValidationFailed"] = "The form validation could not pass. Please check your input.";

        return CurrentUmbracoPage();
      }

      TempData["TripQuoteFormValidationPasses"] = "The form has been validated successfully.";
      TempData["TripQuoteFormFolderId"] = FormFolderId;

      SaveFormSubmission(model);
      SendEmailNotifications(model);

      var formFolder = Umbraco.TypedContent(FormFolderId);

      if (formFolder != null && formFolder.HasValue("redirectPage"))
      {
        return RedirectToUmbracoPage(formFolder.GetPropertyValue<int>("redirectPage"));
      }

      return RedirectToCurrentUmbracoPage();
    }

    private void SaveFormSubmission(TripQuoteForm model)
    {
      try
      {
        var contentService = Services.ContentService;
        var formSubmission = contentService.CreateContent(model.RecipientName + ", " + model.RecipientEmail + " - " + DateTime.Now.ToShortDateString(), FormFolderId, "tripQuoteForm");

        formSubmission.SetValue("tripName", model.TripName);
        formSubmission.SetValue("duration", model.Duration);
        formSubmission.SetValue("basePrice", model.BasePrice);

        formSubmission.SetValue("departureDate", model.DepartureDate);
        formSubmission.SetValue("returnDate", model.ReturnDate);
        formSubmission.SetValue("studentNumber", model.StudentNumber);
        formSubmission.SetValue("groupLeaderNumber", model.GroupLeaderNumber);
        formSubmission.SetValue("travelMethod", model.TravelMethod);
        formSubmission.SetValue("studentAgeLower", model.StudentAgeLower);
        formSubmission.SetValue("studentAgeUpper", model.StudentAgeUpper);

        formSubmission.SetValue("enhanceTrip", model.EnhanceTrip);

        formSubmission.SetValue("schoolName", model.SchoolName);
        formSubmission.SetValue("address", model.Address);
        formSubmission.SetValue("street", model.Street);
        formSubmission.SetValue("town", model.Town);
        formSubmission.SetValue("country", model.Country);
        formSubmission.SetValue("postcode", model.Postcode);

        formSubmission.SetValue("recipientName", model.RecipientName);
        formSubmission.SetValue("recipientEmail", model.RecipientEmail);
        formSubmission.SetValue("recipientTelephone", model.RecipientTelephone);

        contentService.SaveAndPublishWithStatus(formSubmission);
      }
      catch (Exception ex)
      {
        LogHelper.Warn(GetType(), "Trip Quote form saving failed with the exception: " + ex.Message);
      }
    }

    private void SendEmailNotifications(TripQuoteForm model)
    {
      var formFolder = Umbraco.TypedContent(FormFolderId);

      if (formFolder != null)
      {
        _mailHelper.CreateAndSendNotifications(model, formFolder);
      }
      else
      {
        LogHelper.Warn(GetType(), "Couldn't get the form folder with the id: " + FormFolderId);
      }
    }

  }
}

