﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;
using StudyTripsFinder.Forms.Models;
using StudyTripsFinder.Forms.Utility;
using TGDH.Core.Utility;

namespace StudyTripsFinder.Forms.Controllers
{
    public class GatedContentController  : SurfaceController
    {
        private readonly MailHelper _mailHelper = new MailHelper();
        private const int FormFolderId = 0;
        private const string cookieKey = "stf-gated-access";
       
        public ActionResult RenderContactForm()
        {
            return PartialView("~/Views/Partials/Forms/GatedContentFormView.cshtml", new GatedContentForm());
        }

        public static bool HasGatedAccess(bool isGatedPage = false)
        {
            if (!isGatedPage)
            {
                return true;
            }
            return CookieHelper.GetCookie(cookieKey) != null;
        }

        public static void SetGatedAccess() 
        {
            CookieHelper.SetCookie(cookieKey,"true");
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessFormSubmission(GatedContentForm model)
        {
            if (!ModelState.IsValid)
            {
                TempData["GatedContentFormValidationFailed"] = "The form validation could not pass. Please check your input.";

                return CurrentUmbracoPage();
            }

            TempData["GatedContentFormValidationPasses"] = "The form has been validated successfully.";
            TempData["GatedContentFormFormFolderId"] = FormFolderId;

            SetGatedAccess();

            //SaveContactFormSubmission(model);

            var formFolder = Umbraco.TypedContent(FormFolderId);

            return RedirectToCurrentUmbracoPage();
        }
    }
}