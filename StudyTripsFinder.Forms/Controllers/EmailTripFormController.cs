﻿using System;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using System.Collections.Generic;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using StudyTripsFinder.Forms.Models;
using StudyTripsFinder.Forms.Utility;
using System.Net.Mail;
using Umbraco.Core.Models;
using System.Web;
using System.Linq;


namespace StudyTripsFinder.Forms.Controllers
{
  public class EmailTripFormController : SurfaceController
  {
    private readonly MailHelper _mailHelper = new MailHelper();
    private const int FormFolderId = 1336;

    public ActionResult RenderEmailTripForm(string tripName, string priceFrom, string duration, List<CheckboxItem> enhanceTripOptions)
    {
      return PartialView("~/Views/Partials/Forms/EmailTripFormView.cshtml", new EmailTripForm
      {
        TripName = tripName,
        BasePrice = priceFrom,
        EnhanceTripOptions = enhanceTripOptions
      });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult ProcessFormSubmission(EmailTripForm model)
    {
      /*            
                  string userResponse = HttpContext.Request.Params["g-recaptcha-response"];
                  bool validCaptcha = ReCaptcha.ValidateCaptcha(userResponse);
      */
      /* 
                  if (!validCaptcha)
                  {
                      TempData["EmailTripFormValidationFailed"] = "The form did not pass the Google ReCaptcha validation.";

                      return CurrentUmbracoPage();
                  }
       */
      var formFolder = Umbraco.TypedContent(FormFolderId);

      if (!ModelState.IsValid)
      {
        TempData["EmailTripFormValidationFailed"] = "The form validation could not pass. Please check your input.";

        return CurrentUmbracoPage();
      }

      TempData["EmailTripFormValidationPasses"] = "The form has been validated successfully.";
      TempData["EmailTripFormFolderId"] = FormFolderId;

      CreateAndSendEmails(model, formFolder);

      return RedirectToCurrentUmbracoPage();
    }


    private void CreateAndSendEmails(EmailTripForm model, IPublishedContent formFolder)
    {
      var emailTemplate = _mailHelper.LoadEmailTemplate("EmailTemplate");
      var emailBody = formFolder.GetPropertyValue<string>("emailBody");
      var valuesToReplace = _mailHelper.CreateValuesToReplace(model);

      emailBody = _mailHelper.ReplacePlaceholders(emailBody, valuesToReplace);

      var finishedEmail = emailTemplate.Replace("{{Content}}", emailBody);

      if (string.IsNullOrEmpty(emailBody)) return;

      var senderName = formFolder.GetPropertyValue<string>("senderName");
      var subject = formFolder.GetPropertyValue<string>("subject");
      var fromAddress = formFolder.GetPropertyValue<string>("fromAddress");

      var mailMessages = new List<MailMessage>();

      if (_mailHelper.IsValidEmail(model.RecipientEmail))
      {
        mailMessages.Add(_mailHelper.CreateMailMessage(finishedEmail, subject, model.RecipientEmail, fromAddress));
      }

      if (model.AdditionalEmails != null && model.AdditionalEmails.Count > 0)
      {
        foreach (var email in model.AdditionalEmails)
        {
          if (!_mailHelper.IsValidEmail(email)) continue;
          mailMessages.Add(_mailHelper.CreateMailMessage(finishedEmail, subject, email, fromAddress));
        }
      }

      if (!mailMessages.Any()) return;

      foreach (var mailMessage in mailMessages)
      {
        _mailHelper.SendMailMessage(mailMessage);
      }
    }

  }
}