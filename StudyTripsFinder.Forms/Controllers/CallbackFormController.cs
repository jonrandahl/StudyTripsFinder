﻿using System;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using StudyTripsFinder.Forms.Models;
using StudyTripsFinder.Forms.Utility;
using hbehr.recaptcha;

namespace StudyTripsFinder.Forms.Controllers
{
    public class CallbackFormController : SurfaceController
    {
        private readonly MailHelper _mailHelper = new MailHelper();
        private const int FormFolderId = 1319;

        public ActionResult RenderCallbackForm(string pageName = "")
        {
            return PartialView("~/Views/Partials/Forms/CallbackFormView.cshtml", new CallbackForm {
                PageName = pageName
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessFormSubmission(CallbackForm model)
        {
            string userResponse = HttpContext.Request.Params["g-recaptcha-response"];
            bool validCaptcha = ReCaptcha.ValidateCaptcha(userResponse);

            if (!validCaptcha)
            {
                TempData["CallbackFormValidationFailed"] = "The form did not pass the Google ReCaptcha validation.";

                return CurrentUmbracoPage();
            }

            if (!ModelState.IsValid)
            {
                TempData["CallbackFormValidationFailed"] = "The form validation could not pass. Please check your input.";

                return CurrentUmbracoPage();
            }

            TempData["CallbackFormValidationPasses"] = "The form has been validated successfully.";
            TempData["CallbackFormFormFolderId"] = FormFolderId;

            SaveCallbackFormSubmission(model);
            SendEmailNotifications(model);

            var formFolder = Umbraco.TypedContent(FormFolderId);

            if (formFolder != null && formFolder.HasValue("redirectPage"))
            {
                return RedirectToUmbracoPage(formFolder.GetPropertyValue<int>("redirectPage"));
            }

            return RedirectToCurrentUmbracoPage();
        }

        private void SaveCallbackFormSubmission(CallbackForm model)
        {
            try
            {
                var contentService = Services.ContentService;
                var formSubmission = contentService.CreateContent(model.SenderName + ", " + model.Email + " - " + DateTime.Now.ToShortDateString(), FormFolderId, "callbackForm");

                formSubmission.SetValue("pageName", model.PageName);
                formSubmission.SetValue("senderName", model.SenderName);
                formSubmission.SetValue("emailAddress", model.Email);
                formSubmission.SetValue("telephone", model.Telephone);
                formSubmission.SetValue("schoolName", model.SchoolName);

                contentService.SaveAndPublishWithStatus(formSubmission);
            }
            catch (Exception ex)
            {
                LogHelper.Warn(GetType(), "Callback form saving failed with the exception: " + ex.Message);
            }

        }

        private void SendEmailNotifications(CallbackForm model)
        {
            var formFolder = Umbraco.TypedContent(FormFolderId);

            if (formFolder != null)
            {
                _mailHelper.CreateAndSendNotifications(model, formFolder);
            }
            else
            {
                LogHelper.Warn(GetType(), "Couldn't get the form folder with the id: " + FormFolderId);
            }
        }

    }
}