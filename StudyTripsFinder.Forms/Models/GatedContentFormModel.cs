﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StudyTripsFinder.Forms.Models
{
    public class GatedContentForm
    {
        [DisplayName("Page name")]
        [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
        public string PageName { get; set; }

        [DisplayName("Email")]
        [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }
    }
}