﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StudyTripsFinder.Forms.Models
{
  public class ContactForm
  {
    [DisplayName("Page name")]
    //[MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string PageName { get; set; }

    [Required(ErrorMessage = "Please enter your name")]
    [DisplayName("Sender name")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string SenderName { get; set; }

    [Required(ErrorMessage = "Please enter your email address")]
    [DisplayName("Email")]
    [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid email address")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Email { get; set; }

    [DisplayName("Message")]
    [Required(ErrorMessage = "Please enter your message")]
    public string Message { get; set; }

    public string RecaptchaPublicKey { get; }

    public ContactForm() { }

    public ContactForm(string recaptchaPublicKey)
    {
      RecaptchaPublicKey = recaptchaPublicKey;
    }
  }
}