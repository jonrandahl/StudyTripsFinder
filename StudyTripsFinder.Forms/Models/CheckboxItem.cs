﻿namespace StudyTripsFinder.Forms.Models
{
    public class CheckboxItem
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Subtitle { get; set; }

        public bool Checked { get; set; }
    }
}