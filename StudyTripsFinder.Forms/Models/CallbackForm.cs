﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StudyTripsFinder.Forms.Models
{
  public class CallbackForm
  {
    [DisplayName("Page name")]
    //[MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string PageName { get; set; }

    [Required(ErrorMessage = "Please enter your name")]
    [DisplayName("Sender name")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string SenderName { get; set; }

    [Required(ErrorMessage = "Please enter your email address")]
    [DisplayName("Email")]
    [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid email address")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Email { get; set; }

    //[Required(ErrorMessage = "Please enter your school name")]
    [DisplayName("School name")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string SchoolName { get; set; }

    //[Required(ErrorMessage = "Please enter your telephone number")]
    [DisplayName("Telephone")]
    [DataType(DataType.PhoneNumber, ErrorMessage = "Provided phone number not valid")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Telephone { get; set; }

    public string RecaptchaPublicKey { get; }

    public CallbackForm() { }

    public CallbackForm(string recaptchaPublicKey)
    {
      RecaptchaPublicKey = recaptchaPublicKey;
    }
  }
}