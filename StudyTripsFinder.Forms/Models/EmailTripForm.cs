﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;

namespace StudyTripsFinder.Forms.Models
{
  public class EmailTripForm
  {
    [DisplayName("Enhance trip options")]
    public List<CheckboxItem> EnhanceTripOptions { get; set; }

    [DisplayName("Travel method options")]
    public List<string> TravelMethodOptions { get; set; }

    [DisplayName("Trip name")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string TripName { get; set; }

    [DisplayName("Duration")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Duration { get; set; }

    [DisplayName("Accommodation")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Accommodation { get; set; }

    [DisplayName("Base price")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string BasePrice { get; set; }

    // trip details
    [DisplayName("Departure date")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string DepartureDate { get; set; }

    [DisplayName("Return date")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string ReturnDate { get; set; }

    [DisplayName("Number of students")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string StudentNumber { get; set; }

    [DisplayName("Number of group leaders")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string GroupLeaderNumber { get; set; }

    [DisplayName("Method of travel")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string TravelMethod { get; set; }

    [DisplayName("Pupil / Student lower age")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string StudentAgeLower { get; set; }

    [DisplayName("Pupil / Student upper age")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string StudentAgeUpper { get; set; }

    // enhance trip
    public string EnhanceTrip { get; set; }

    // school details
    [DisplayName("School name")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string SchoolName { get; set; }

    [DisplayName("Address")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Address { get; set; }

    [DisplayName("Street")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Street { get; set; }

    [DisplayName("Town")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Town { get; set; }

    [DisplayName("Country")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Country { get; set; }

    [DisplayName("Postcode")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string Postcode { get; set; }

    // recipient details
    [DisplayName("Name")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string RecipientName { get; set; }

    [DisplayName("Email")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    [Required(ErrorMessage = "Please enter your email address")]
    [EmailAddress(ErrorMessage = "Please enter a valid email address")]
    public string RecipientEmail { get; set; }

    [DisplayName("Telephone number")]
    [MaxLength(254, ErrorMessage = "Field exceeded maximum length")]
    public string RecipientTelephone { get; set; }

    [DisplayName("Email address")]
    public List<string> AdditionalEmails { get; set; }

    public string RecaptchaPublicKey { get; }

    public EmailTripForm()
    {
      TravelMethodOptions = new List<string>(new[] { "Air", "Coach", "Eurostar", "Train" });
    }
    public EmailTripForm(string tripName, string priceFrom, string recaptchaPublicKey, List<CheckboxItem> enhanceTripOptions)
    {
      TripName = tripName;
      BasePrice = priceFrom;
      EnhanceTripOptions = enhanceTripOptions;
      RecaptchaPublicKey = recaptchaPublicKey;
      TravelMethodOptions = new List<string>(new[] { "Air", "Coach", "Eurostar", "Train" });
    }

  }
}