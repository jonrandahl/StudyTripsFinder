﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudyTripsFinder.Forms.Models;
using Umbraco.Core;
using Umbraco.Core.Logging;

namespace StudyTripsFinder.Forms.Utility
{
    public class Helper
    {
        public static string GetResultsFromList(List<CheckboxItem> selection)
        {
            if (selection == null || !selection.Any())
            {
                return "N/A";
            }

            var stringToReturn = "";

            foreach (var item in selection)
            {
                LogHelper.Warn(item.GetType(), "[TGDH]: " + item.Name);
                if (item.Checked)
                {
                    if (String.IsNullOrWhiteSpace(stringToReturn))
                    {
                        stringToReturn = item.Name + ", ";
                    }
                    else
                    {
                        stringToReturn = stringToReturn + System.Environment.NewLine + item.Name + ", ";
                    }
                }
            }

            if (String.IsNullOrWhiteSpace(stringToReturn))
            {
                stringToReturn = "N/A";
            }

            return stringToReturn.TrimEnd(", ");
        }
    }
}