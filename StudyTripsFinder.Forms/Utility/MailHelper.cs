﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Web;
using Umbraco.Web;
using System.Reflection;
using System.Text;
using Umbraco.Core.Models;

namespace StudyTripsFinder.Forms.Utility
{
    public class MailHelper
    {
        public string LoadEmailTemplate(string emailtemplateName)
        {
            string emailTemplate;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Code/" + emailtemplateName + ".html")))
            {
                emailTemplate = reader.ReadToEnd();
            }

            return emailTemplate;
        }

        public Dictionary<string, string> CreateValuesToReplace(object model)
        {
            var type = model.GetType();
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var valuesToReplace = new Dictionary<string, string>();
            foreach (var property in properties)
            {
                if (property.PropertyType == typeof(string))
                {
                    var propValue = (string)type.GetProperty(property.Name).GetValue(model, null);
                    if (propValue != null && !String.IsNullOrEmpty(propValue)) {
                        valuesToReplace.Add("{{" + property.Name + "}}", (string)type.GetProperty(property.Name).GetValue(model, null).ToString());
                    } else {
                        valuesToReplace.Add("{{" + property.Name + "}}", "N/A");
                    }
                }

            }

            return valuesToReplace;
        }

        public string ReplacePlaceholders(string emailBody, Dictionary<string, string> valuesToReplace)
        {
            foreach (var value in valuesToReplace)
            {
                emailBody = emailBody.Replace(value.Key, value.Value);
            }

            return emailBody;
        }

        public MailMessage CreateMailMessage(string finishedEmail, string subject, string emailAddress, string fromAddress)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress(fromAddress),
                To = { emailAddress },
                Subject = subject,
                Body = finishedEmail,
                IsBodyHtml = true
            };

            return mailMessage;
        }

        public MailMessage SendMailMessage(MailMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                client.Send(mailMessage);
                mailMessage.Dispose();
            }

            return mailMessage;
            
        }

        public bool IsValidEmail(string email)
        {
            bool isValid;

            try
            {
                var addr = new MailAddress(email);
                isValid = addr.Address == email;
            }
            catch
            {
                isValid = false;
            }

            return isValid;
        }

        /*
         * 
         */

        public void CreateAndSendNotifications(object model, IPublishedContent formFolder)
        {
            var emailTemplate = LoadEmailTemplate("EmailTemplate");

            if (!string.IsNullOrEmpty(formFolder.GetPropertyValue<string>("internalNotificationAddress")))
            {
                var email = formFolder.GetPropertyValue<string>("internalNotificationAddress");
                var emailBody = GenerateMessageFromModel(model);
                var isValidEmail = IsValidEmail(email);

                if (string.IsNullOrEmpty(emailBody) || !isValidEmail) return;

                var finishedEmail = emailTemplate.Replace("{{Content}}", emailBody);
                var mailMessage = CreateInternalMailMessage(finishedEmail, formFolder, email);

                SendMailMessage(mailMessage);
            }

            if (!formFolder.GetPropertyValue<bool>("sendNotification")) return;
            {
                var type = model.GetType();
                var email = (string)type.GetProperty("Email").GetValue(model, null);
                var emailBody = formFolder.GetPropertyValue<IHtmlString>("notificationMessage").ToString();
                var valuesToReplace = CreateValuesToReplace(model);

                emailBody = ReplacePlaceholders(emailBody, valuesToReplace);

                if (string.IsNullOrEmpty(emailBody)) return;

                var finishedEmail = emailTemplate.Replace("{{Content}}", emailBody);
                var mailMessage = CreateExternalMailMessage(finishedEmail, formFolder, email);

                SendMailMessage(mailMessage);
            }
        }

        private MailMessage CreateInternalMailMessage(string emailBody, IPublishedContent formFolder, string contactAddress)
        {
            var fromAddress = formFolder.HasValue("fromAddress")
                ? formFolder.GetPropertyValue<string>("fromAddress")
                : "noreply@" + HttpContext.Current.Request.Url.Host;
            var senderName = formFolder.GetPropertyValue<string>("senderName");
            var emailSubject = formFolder.Name + " has been submitted by " + contactAddress;
            var emailCcAddresses = formFolder.GetPropertyValue<string>("internalNotificationCc");

            var mailMessage = new MailMessage
            {
                From = new MailAddress(fromAddress, senderName),
                Subject = emailSubject,
                Body = emailBody,
                IsBodyHtml = true,
                To =
                {
                    contactAddress
                }
            };

            if (string.IsNullOrEmpty(emailCcAddresses)) return mailMessage;

            foreach (var emailAddress in emailCcAddresses.Split(','))
            {
                if (IsValidEmail(emailAddress))
                {
                    mailMessage.CC.Add(emailAddress);
                }
            }

            return mailMessage;
        }

        public string GenerateMessageFromModel(object model)
        {
            var emailBody = new StringBuilder();

            var type = model.GetType();
            var properties = type.GetProperties();

            emailBody.Append("<h2>Summary of the form: </h2>");

            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.CanRead)
                {
                    if (propertyInfo.PropertyType == typeof(string) || propertyInfo.PropertyType == typeof(Boolean) || propertyInfo.PropertyType == typeof(Int32))
                    {
                        if (propertyInfo.GetValue(model, null) != null)
                        {
                            emailBody.Append("<p><strong>" + DisplayNameHelper.GetDisplayName(propertyInfo) + ": </strong>" + propertyInfo.GetValue(model, null) + "</p>");
                        }
                    }
                }
            }

            return emailBody.ToString();
        }

        private MailMessage CreateExternalMailMessage(string emailBody, IPublishedContent formFolder, string contactAddress)
        {
            var fromAddress = formFolder.HasValue("fromAddress")
                ? formFolder.GetPropertyValue<string>("fromAddress")
                : "noreply@" + HttpContext.Current.Request.Url.Host;
            var senderName = formFolder.GetPropertyValue<string>("senderName");
            var emailSubject = formFolder.HasValue("notificationTitle")
                ? formFolder.GetPropertyValue<string>("notificationTitle")
                : "Your form has been received";

            var mailMessage = new MailMessage
            {
                From = new MailAddress(fromAddress, senderName),
                Subject = emailSubject,
                Body = emailBody,
                IsBodyHtml = true,
                To =
                {
                    contactAddress
                }
            };

            return mailMessage;
        }
    }
}
