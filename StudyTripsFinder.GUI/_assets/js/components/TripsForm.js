var TripsForm = (function ($) {
	'use strict';

	var $form;
	var $selectInputs;

	var queryParams;
	var baseUrl = "/trips";

	var _getQueryStringAsString = function(link) {
		var query = "";

		if (link.indexOf("?") >= 0) {
			query = link.substr(link.indexOf("?") + 1);
		}

		return query;
	};

	//https://css-tricks.com/snippets/jquery/get-query-params-object/
	var _getQueryStringParams = function(str) {
		/* jshint ignore:start */
		return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
		/* jshint ignore:end */
	};

	var _updateFormAction = function(e) {
		var link = $(e.target).val();
		link = _getQueryStringAsString(link);
		var selectedParams = _getQueryStringParams(link);

		$.extend( queryParams, selectedParams );
		$form.attr("action", baseUrl + "?" + $.param(queryParams));

	};

	var _handleSubmit = function(e) {
		window.location = $form.attr('action');
		e.preventDefault();

	};

	var _addEventListeners = function() {
		$form.on('submit', function(e) {
			_handleSubmit(e);
		});

		$selectInputs.on('change', function(e) {
			_updateFormAction(e);
		});
	};

	var init = function( form ) {
		if (form.length === 0) {
			return;
		}
		$form = form;
		$selectInputs = $form.find('select');
		queryParams = _getQueryStringParams();

		_addEventListeners();
	};

	return {
		init: init
	};

})(jQuery);
