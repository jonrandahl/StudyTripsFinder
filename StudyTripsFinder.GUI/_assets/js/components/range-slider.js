var RangeSlider = (function ($) {
	'use strict';

	var $slider;
	var $inputs = [];
	var sliderOpts = {};

	var _setupSlider = function($container) {
		$slider = $container.find('.js-range-slider');
		$inputs = [
			$container.find('.js-range-slider-value-lower'),
			$container.find('.js-range-slider-value-upper')
		];
		sliderOpts = {
			start: $slider.data("start"),
			end: $slider.data("end"),
			step: $slider.data("step"),
			min: $slider.data("min"),
			max: $slider.data("max")
		};

		noUiSlider.create($slider[0], {
			start: [sliderOpts.start, sliderOpts.end],
			step: sliderOpts.step,
			connect: true,
			range: {
				'min': sliderOpts.min,
				'max': sliderOpts.max
			}
		});

		_bindEvents();
	};

	var _update = function(values, handle) {
		var value = values[handle];
		value = value.substring(0, value.indexOf("."));

		$inputs[handle].each(function() {
			$(this).text(value);
			$(this).val(value);
		});


	};

	var _bindEvents = function() {
		$slider[0].noUiSlider.on("update", function(values, handle) {
			_update(values, handle);
		});
	};

	var _init = function( containers ) {
		if ( containers.length === 0 ) return;

		containers.each(function() {
			_setupSlider($(this));
		});

	};

	return {
		init: _init
	};

})(jQuery);
