var CheckboxGroup = (function ($) {
	'use strict';

	// the checkbox that was clicked
	var $container;
	var $actor;
	var groupName;
	var $group;
	var $actorSiblings;
	var val;

	var _toggleGroup = function(el) {
		$actor = el;
		val = $actor.prop('checked');
		groupName = $actor.data('group');
		$group = $('input[type="checkbox"][data-group="' + groupName + '"]');
		$actorSiblings = $group.prop('checked', val).change();
	};

	var _bindEvents = function(containers) {
		containers.on('click', 'input[type="checkbox"][data-group]', function(e) {
			_toggleGroup($(this));
		});
	};

	var init = function( containers ) {
		if ( containers.length === 0 ) return;

		_bindEvents(containers);
	};

	return {
		init: init
	};

})(jQuery);
