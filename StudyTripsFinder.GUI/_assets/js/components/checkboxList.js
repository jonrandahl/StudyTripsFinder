var CheckboxList = (function ($) {
	'use strict';

	var $container;
	var $input;
	var $checkboxes;
	var string;

	var _updateInput = function() {
		$input.val(string);
	};

	var _updateString = function() {
		string = $container.find(':checkbox').filter(":checked").map(function(i,v){
            return this.value;
        }).get().join(" , ");
	};

	var _bindEvents = function(containers) {
		$checkboxes.on('change', function(e) {
			$container = $(this).closest('.js-checkbox-list');
			_updateString();
			_updateInput();
		});
	};

	var init = function(trigger) {
		$container = trigger;
		if ($container.length === 0) return;

		$input = $container.find('.js-checkbox-list-input');
		$checkboxes = $container.find(':checkbox');

		_bindEvents();
	};

	return {
		init: init
	};

})(jQuery);
