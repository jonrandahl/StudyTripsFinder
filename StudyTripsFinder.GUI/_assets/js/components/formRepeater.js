var FormFieldRepeater = (function($) {
    'use strict';

    var $container;
	var $addButton;
    var $repeaterItemElement;
	var $repeaterTemplateItem;
	var counter = 0;

	var _cloneItem = function() {
        var $repeaterItem = $repeaterItemElement;

        $repeaterTemplateItem = $repeaterItem.clone();
        $repeaterItem.remove();
    };

    var _createItem = function() {
        var $newItem = $repeaterTemplateItem.clone();

        _appendItemToDom($newItem);
    };

    var _updateItemIds = function($item, count) {
        var $fields = $item.find('label, input, textarea, span');

        $fields.each(function() {
            $.each(this.attributes, function(i, attrib) {
                var name = attrib.name;

                if (name !== "rows") {
                    var value = attrib.value;
                    var number = value.match(/\d+/);

                    attrib.value = value.replace(number, count);
                }
            });
        });
    };

    var _addItem = function() {
        counter++;

        _createItem();
        _optimiseIndexNumbers();
    };

    var _optimiseIndexNumbers = function() {
        $container.find('.js-form-repeater__item').each(function(index, el) {
            var $this = $(el);

            _updateItemIds($this, index);
        });
    };

    var _appendItemToDom = function($item) {
        $item.insertBefore($addButton);

        _bindEvents($item);
        _hideSiblings($item);
    };

    var _removeItem = function($item) {
        counter--;

        $item.remove();
        _optimiseIndexNumbers();
    };

    var _hideSiblings = function($item) {
        $item.siblings().find('.js-form-repeater__inner').hide();
    };

    var _bindEvents = function($item) {

        $item.find('.js-form-repeater__remove').on('click', function() {
            _removeItem($item);
        });

	};

    var _init = function() {
		$container = $('.js-form-repeater');
		if ($container.length === 0) return;

        $addButton = $container.find('.js-form-repeater__add');
        $repeaterItemElement = $container.find('.js-form-repeater__item');

        _cloneItem();

        $addButton.on('click', function() {
            _addItem();
        });

    };

    return {
        init: _init
    };

})(jQuery);
