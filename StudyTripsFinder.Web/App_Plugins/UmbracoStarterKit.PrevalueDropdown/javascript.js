﻿angular.module("umbraco.resources").factory("UmbracoStarterKitPrevalueDropdownResources",
    function ($http) {
        return {
            getPrevalues: function (id) {
                return $http.get("/umbraco/surface/prevalue/getprevalues/" + id);
            }
        };
    }
);

angular.module("umbraco").controller("UmbracoStarterKitPrevalueDropdown", function ($scope, UmbracoStarterKitPrevalueDropdownResources) {

    if ($scope.model.value === null || $scope.model.value === undefined) {
        $scope.model.value = "";
    }

    UmbracoStarterKitPrevalueDropdownResources.getPrevalues($scope.model.config.parentId).then(function (response) {
        $scope.model.prevalues = response.data;
    });
});

