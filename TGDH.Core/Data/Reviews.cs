﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Core;

namespace TGDH.Core.Data
{
    public static class Reviews
    {
        public static List<IPublishedContent> AllReviews(UmbracoHelper umbraco)
        {
            var root = umbraco.TypedContentSingleAtXPath("//reviews");
            var reviews = new List<IPublishedContent>();

            if (root == null)
            {
                return reviews;
            }

            reviews = root.Children().Where(x => x.IsVisible() && x.TemplateId > 0).ToList();
    
            return reviews;
        }

        public static List<IPublishedContent> AllOrderedReviews(UmbracoHelper umbraco)
        {
            return AllReviews(umbraco).OrderByDescending(x => x.GetPropertyValue<DateTime>("releaseDate")).ToList();
        }
        
        public static List<IPublishedContent> GetFiltered(
            List<IPublishedContent> selection,
            string subject = "", 
            string country = "", 
            string city = "",
            string month = "", 
            string year = ""
        ) {
            selection = DataHelpers.FilterBySubject(selection.ToList(), subject);
            selection = DataHelpers.FilterByLocation(selection.ToList(), country, city);

            if (!string.IsNullOrWhiteSpace(year))
            {
                selection = DataHelpers.FilterByYearAndMonth(selection, month, year, "releaseDate");
            }
            return selection.ToList();
        }

    }
}