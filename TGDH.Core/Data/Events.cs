﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace TGDH.Core.Data
{
    public static class Events
    {
        public static List<IPublishedContent> AllEvents(UmbracoHelper umbraco)
        {
            var root = umbraco.TypedContentSingleAtXPath("//events");
            var events = new List<IPublishedContent>();

            if (root == null)
            {
                return events;
            }

            events = root.Children().Where(x => x.IsVisible() && x.TemplateId > 0).ToList();
    
            return events;
        }

        public static List<IPublishedContent> AllOrderedEvents(UmbracoHelper umbraco)
        {
            return AllEvents(umbraco).OrderBy(x => x.GetPropertyValue<DateTime>("releaseDate")).ToList();
        }

        public static List<IPublishedContent> AllUpcomingEvents(UmbracoHelper umbraco)
        {
            var ci = new System.Globalization.CultureInfo("en-GB");
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;

            var today = DateTime.Today;

            return AllOrderedEvents(umbraco).Where(x => x.GetPropertyValue<DateTime>("releaseDate").Date >= today).ToList();
        }

        public static List<IPublishedContent> FilterSelection(List<IPublishedContent> source, string month, string year)
        {

            if (!string.IsNullOrWhiteSpace(year))
            {
                source = DataHelpers.FilterByYearAndMonth(source, month, year, "releaseDate");
            }

            return source;
        }

        public static List<IPublishedContent> GetFiltered(
            List<IPublishedContent> selection,
            string subject = "", 
            string country = "", 
            string city = "", 
            string month = "", 
            string year = ""
        ) {
            selection = DataHelpers.FilterBySubject(selection.ToList(), subject);
            selection = DataHelpers.FilterByLocation(selection.ToList(), country, city);

            if (!string.IsNullOrWhiteSpace(year))
            {
                selection = DataHelpers.FilterByYearAndMonth(selection, month, year, "releaseDate");
            }
            return selection.ToList();
        }

    }
}