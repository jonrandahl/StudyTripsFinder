﻿using System;
using System.Collections.Generic;
using System.Linq;
using TGDH.Core.ExtensionMethods;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Core;

namespace TGDH.Core.Data
{
    public static class DataHelpers
    {
        /// <summary>
        /// Takes a list of nodes and checks if the passed in urlSegment(string)
        /// matches any of the names in the list
        /// </summary>
        /// <param name="source">Selection of nodes</param>
        /// <param name="propertyAlias">Property alias for MNTP to check url segment against</param>
        /// <param name="name">Name to be used as url segment for comparison</param>
        /// <returns>True if any match else false</returns>
        public static List<IPublishedContent> FilterByPrevalueName(List<IPublishedContent> source, string propertyAlias, string name)
        {
            var urlFriendlyName = name.ToUrlSegment();
            return source.Where(x => IsUrlSegmentInList(x.GetPropertyValue<List<IPublishedContent>>(propertyAlias),urlFriendlyName)).ToList();
        }

        /// <summary>
        /// Takes a list of nodes and checks if the passed in urlSegment(string)
        /// matches any of the names in the list
        /// </summary>
        /// <param name="source">Selection of nodes</param>
        /// <param name="urlSegment">Specified url segment to compare</param>
        /// <returns>True if any match else false</returns>
        public static bool IsUrlSegmentInList(List<IPublishedContent> source, string urlSegment) {
            if (source == null)
            {
                return false;
            }
            return source.Any(x => x.Name.ToUrlSegment().InvariantEquals(urlSegment));
        }

        public static List<IPublishedContent> FilterByDocumentTypeAlias(List<IPublishedContent> source, string documentType)
        {
            return source.Where(x => x.DocumentTypeAlias.InvariantEquals(documentType)).ToList();
        }

        public static List<IPublishedContent> FilterBySelectedPrevaluePage(List<IPublishedContent> source, string propertyAlias, IPublishedContent page)
        {
            //return source.Where(x => x.GetPropertyValue<int>(propertyAlias) == page.Id).ToList();
            return source.Where(x => x == page ).ToList();
        }

        public static List<IPublishedContent> FilterByYearAndMonth(List<IPublishedContent> source, string month, string year, string propertyAlias)
        {
            int yearInt;
            var monthInt = IntExtensionMethods.GetMonthNumber(month);
            var isValidYear = int.TryParse(year, out yearInt);

            if (!isValidYear) return source;

            source = source.Where(x => x.GetPropertyValue<DateTime>(propertyAlias).Year == yearInt).ToList();

            if (monthInt > 0)
            {
                source = source.Where(x => x.GetPropertyValue<DateTime>(propertyAlias).Month == monthInt).ToList();
            }

            return source;
        }

        public static List<IPublishedContent> GetListing(UmbracoHelper umbraco, string docType)
        {
            var selection = new List<IPublishedContent>();

            if (String.IsNullOrEmpty(docType)) {
                return selection;
            }

            var root = umbraco.TypedContentSingleAtXPath("//" + docType);

            if (root == null)
            {
                return selection;
            }

            selection = root.Children().Where(x => x.IsVisible() && x.TemplateId > 0).ToList();
    
            return selection;

        }

        public static List<IPublishedContent> GetResultsFromMultiDocType(UmbracoHelper umbraco, string docTypes)
        {
            var selection = new List<IPublishedContent>();
            foreach (var docType in docTypes.Split(',')) 
            {
                var children = GetListing(umbraco, docType);
                if(children != null && children.Any()) {
                    selection = selection.Concat(children).ToList();
                }
                
            }
            return selection;
        }

        /* */

        public static List<IPublishedContent> FilterBySubject(List<IPublishedContent> selection, string subject)
        {
            if (string.IsNullOrEmpty(subject)) return selection;

            selection = selection.Where(x => x.GetPropertyValue<string>("subject").ToUrlSegment().InvariantEquals(subject)).ToList();

            return selection;
        }

        public static List<IPublishedContent> FilterByLocation(List<IPublishedContent> selection, string country, string city)
        {
            if (string.IsNullOrEmpty(country) && string.IsNullOrEmpty(city)) return selection;

            var allSelection = new List<IPublishedContent>();

            foreach (var item in selection) {
                var location = item.GetPropertyValue<IEnumerable<IPublishedContent>>("location").FirstOrDefault();
                if (location != null) {
                    var docType = location.DocumentTypeAlias;
                    var isCountry = docType == "country";
                    var isCity = docType == "city";

                    if (isCountry) {
                        if (location.Name.ToUrlSegment().InvariantEquals(country) && String.IsNullOrWhiteSpace(city)) {
                            allSelection.Add(item);
                        }
                    } else if (isCity) {
                        if (String.IsNullOrWhiteSpace(city)) {
                            if (location.Ancestor().Name.ToUrlSegment().InvariantEquals(country)) {
                                allSelection.Add(item);
                            }
                        } else {
                            if (location.Name.ToUrlSegment().InvariantEquals(city)) {
                                allSelection.Add(item);
                            }
                        }
                        
                    }

                }
            }

            return allSelection;
        }

    }
}