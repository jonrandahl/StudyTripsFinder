﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Diplo.Helpers;

namespace TGDH.Core.Data
{
    public class Filters
    {
        public static List<Link> GetCategoryLinks(UmbracoHelper umbraco, string baseUrl = "", string qCat = "")
        {
            var root = umbraco.TypedContentSingleAtXPath("//categories");
            if (root == null)
            {
                return null;
            }
            var categories = root.Children().ToList();

            if (!categories.Any())
            {
                return null;
            }

            var categoryLinks = new List<Link> {
                new Link{
                    Name = "All posts",
                    Url = baseUrl,
                    IsActive = String.IsNullOrWhiteSpace(qCat)
                }
            };

            foreach (var item in categories)
            {
                var linkName = item.Name;
                var urlFriendlyName = linkName.ToUrlSegment();
                var linkUrl = baseUrl + "?category=" + urlFriendlyName;
                var isActive = urlFriendlyName.InvariantEquals(qCat);
                categoryLinks.Add(
                    new Link
                    {
                        Name = linkName,
                        Url = linkUrl,
                        IsActive = isActive
                    });
            }
            return categoryLinks;
        }

        public static List<Link> GetAuthorLinks(UmbracoHelper umbraco, string baseUrl = "", string qAuthor = "")
        {
            var root = umbraco.TypedContentSingleAtXPath("//authors");
            if (root == null)
            {
                return null;
            }
            var authors = root.Children().ToList();

            if (!authors.Any())
            {
                return null;
            }

            var authorLinks = new List<Link> {
                new Link{
                    Name = "All authors",
                    Url = baseUrl,
                    IsActive = String.IsNullOrWhiteSpace(qAuthor)
                }
            };

            foreach (var item in authors)
            {
                var linkName = item.Name;
                var urlFriendlyName = linkName.ToUrlSegment();
                var linkUrl = baseUrl + "?author=" + urlFriendlyName;
                var isActive = urlFriendlyName.InvariantEquals(qAuthor);
                authorLinks.Add(
                    new Link
                    {
                        Name = linkName,
                        Url = linkUrl,
                        IsActive = isActive
                    });
            }
            return authorLinks;
        }

        public static List<Link> GetLocationLinks(UmbracoHelper umbraco, string baseUrl = "", string qCountry = "", string qCity = "")
        {
            var root = umbraco.TypedContentSingleAtXPath("//locations");
            if (root == null)
            {
                return null;
            }

            var locations = root.Children().ToList();

            if (!locations.Any())
            {
                return null;
            }

            var locationLinks = new List<Link> {
                new Link{
                    Name = "All locations",
                    Url = baseUrl,
                    IsActive = String.IsNullOrWhiteSpace(qCountry) && String.IsNullOrWhiteSpace(qCity)
                }
            };
            
            foreach (var country in locations)
            {
                var cities = country.Children();

                var countryName = country.Name;
                var countryUrlName = countryName.ToUrlSegment();
                var countryLinkUrl = baseUrl + "?country=" + countryUrlName;
                var isCountryActive = countryUrlName.InvariantEquals(qCountry);
                locationLinks.Add(
                    new Link
                    {
                        Name = countryName,
                        Url = countryLinkUrl,
                        IsActive = isCountryActive,
                        ModifierClass = "c-select__item--level-1"
                    });

                if (cities.Any()) {
                    foreach (var city in cities) {
                        var cityName = city.Name;
                        var cityUrlName = cityName.ToUrlSegment();
                        var cityLinkUrl = baseUrl + "?country=" + countryUrlName + "&city=" + cityUrlName;
                        var isCityActive = cityUrlName.InvariantEquals(qCity);
                        var cityActiveClass = isCountryActive && isCityActive ? "selected" : "";

                        locationLinks.Add(
                            new Link
                            {
                                Name = cityName,
                                Url = cityLinkUrl,
                                IsActive = isCityActive,
                                ModifierClass = "c-select__item--level-2"
                            });

                    }
                }

            }
            return locationLinks;
        }

        public static List<Link> GetDatesFromSelection(List<IPublishedContent> selection = null, string baseUrl = "", string qMonth = "", string qYear = "")
        {
            if (selection == null || !selection.Any())
            {
                return null;
            }

            var dateList = selection.Where(i => i.HasValue("releaseDate"))
                .Select(d => new DateTime(d.GetPropertyValue<DateTime>("releaseDate").Year, d.GetPropertyValue<DateTime>("releaseDate").Month, 1))
                .Distinct()
                .ToList();

            var dateLinks = new List<Link> {
                    new Link{
                        Name = "All dates",
                        Url = baseUrl,
                        IsActive = String.IsNullOrWhiteSpace(qYear) && String.IsNullOrWhiteSpace(qMonth)
                    }
                };

            foreach (var date in dateList)
            {
                var linkName = date.ToString("MMMM yyyy");
                var urlMonth = date.ToString("MMMM").ToLower();
                var urlYear = date.ToString("yyyy");
                var linkUrl = baseUrl + "?month=" + urlMonth + "&year=" + urlYear;
                var isActive = urlMonth.InvariantEquals(qMonth) && urlYear.InvariantEquals(qYear);
                dateLinks.Add(
                    new Link
                    {
                        Name = linkName,
                        Url = linkUrl,
                        IsActive = isActive
                    });
            }
            return dateLinks;
        }

        public static List<Link> GetSubjectLinks(UmbracoHelper umbraco, string baseUrl = "", string qSubject = "")
        {
            var root = umbraco.TypedContentSingleAtXPath("//subjects");
            if (root == null)
            {
                return null;
            }
            var subjects = root.Children().ToList();

            if (!subjects.Any())
            {
                return null;
            }

            var subjectLinks = new List<Link> {
                new Link{
                    Name = "All subjects",
                    Url = baseUrl,
                    IsActive = String.IsNullOrWhiteSpace(qSubject)
                }
            };

            foreach (var item in subjects)
            {
                var linkName = item.Name;
                var urlFriendlyName = linkName.ToUrlSegment();
                var linkUrl = baseUrl + "?subject=" + urlFriendlyName;
                var isActive = urlFriendlyName.InvariantEquals(qSubject);
                subjectLinks.Add(
                    new Link
                    {
                        Name = linkName,
                        Url = linkUrl,
                        IsActive = isActive
                    });
            }
            return subjectLinks;
        }

        public static List<Link> GetSubjectLinksWithQueryString(UmbracoHelper umbraco, string baseUrl = "", string qString = "")
        {
            var root = umbraco.TypedContentSingleAtXPath("//subjects");
            if (root == null)
            {
                return null;
            }

            QueryStringHelper qAll = new QueryStringHelper(qString);
            var qSubject = qAll.GetValueByName("subject");
            qAll.RemoveByName("subject");

            var subjects = root.Children().ToList();

            if (!subjects.Any())
            {
                return null;
            }

            var subjectLinks = new List<Link> {
                new Link{
                    Name = "All subjects",
                    Url = baseUrl + "?subject=&" + qAll.ToString(),
                    IsActive = String.IsNullOrWhiteSpace(qSubject)
                }
            };

            foreach (var item in subjects)
            {
                var linkName = item.Name;
                var urlFriendlyName = linkName.ToUrlSegment();
                qAll.AddOrReplace("subject", urlFriendlyName);
                var linkUrl = baseUrl + "?" + qAll.ToString();
                var isActive = urlFriendlyName.InvariantEquals(qSubject);
                subjectLinks.Add(
                    new Link
                    {
                        Name = linkName,
                        Url = linkUrl,
                        IsActive = isActive
                    });
            }
            return subjectLinks;
        }
        
    }
}