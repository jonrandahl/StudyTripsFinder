﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Examine.LuceneEngine.SearchCriteria;

namespace TGDH.Core.Data
{
    public static class KeepUpToDate
    {

        public static List<IPublishedContent> AllEntries(UmbracoHelper umbraco, string collections = "blog,news")
        {
           var selection = DataHelpers.GetResultsFromMultiDocType(umbraco, collections);

            return selection.Where(x => x.IsVisible() && x.TemplateId > 0).ToList();
        }

        public static IEnumerable<IPublishedContent> AllOrderedEntries(UmbracoHelper umbraco, string collections = "blog,news")
        {
            return AllEntries(umbraco, collections).OrderByDescending(x => x.GetPropertyValue<DateTime>("releaseDate"));
        }

        public static List<IPublishedContent> GetFiltered(string subject = "", string country = "", string city = "", int limit = 0)
        {
            List<IPublishedContent> selection;

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            selection = AllOrderedEntries(umbracoHelper).ToList();
            
            selection = DataHelpers.FilterBySubject(selection, subject);
            selection = DataHelpers.FilterByLocation(selection, country, city);

            if (selection.Any() && limit > 0) {
                selection = selection.Take(limit).ToList();
            }
            return selection;
        }

    }
}