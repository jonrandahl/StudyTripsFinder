﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace TGDH.Core.Data
{
    public static class Blog
    {
        public static List<IPublishedContent> AllPosts(UmbracoHelper umbraco)
        {
            var root = umbraco.TypedContentSingleAtXPath("//blog");
            var posts = new List<IPublishedContent>();

            if (root == null)
            {
                return posts;
            }

            posts = root.Children().Where(x => x.IsVisible() && x.TemplateId > 0).ToList();

            return posts;
        }

        public static List<IPublishedContent> AllOrderedPosts(UmbracoHelper umbraco)
        {
            return AllPosts(umbraco).OrderByDescending(x => x.GetPropertyValue<DateTime>("releaseDate")).ToList();
        }

        public static bool StringInList(string stringInQuestion, string stringList)
        {
            return stringList.Split(',').Any(x => x.Equals(stringInQuestion, StringComparison.OrdinalIgnoreCase));
        }

        public static List<IPublishedContent> FilterSelection(List<IPublishedContent> source, string author, string category, string month, string year)
        {
            var filterByCategory = !string.IsNullOrWhiteSpace(category);
            var filterByAuthor = !string.IsNullOrWhiteSpace(author);

            if (filterByAuthor && filterByCategory)
            {
                var postsInCategory = DataHelpers.FilterByPrevalueName(source, "category", category);
                var postsByAuthor = DataHelpers.FilterByPrevalueName(source, "author", author);

                source = postsInCategory.Intersect(postsByAuthor).ToList();
            }
            else
            {
                if (filterByAuthor)
                {
                    source = DataHelpers.FilterByPrevalueName(source, "author", author);
                }

                if (filterByCategory)
                {
                    source = DataHelpers.FilterByPrevalueName(source, "category", category);
                }
            }

            if (!string.IsNullOrWhiteSpace(year))
            {
                source = DataHelpers.FilterByYearAndMonth(source, month, year, "releaseDate");
            }

            return source;
        }

        public static List<IPublishedContent> GetFiltered(
            List<IPublishedContent> selection,
            string subject = "", 
            string country = "", 
            string city = "", 
            string author = "", 
            string month = "", 
            string year = ""
        ) {
            var filterByAuthor = !String.IsNullOrWhiteSpace(author);

            selection = DataHelpers.FilterBySubject(selection.ToList(), subject);
            selection = DataHelpers.FilterByLocation(selection.ToList(), country, city);
            if (filterByAuthor) {
                selection = DataHelpers.FilterByPrevalueName(selection, "author", author);
            }

            if (!string.IsNullOrWhiteSpace(year))
            {
                selection = DataHelpers.FilterByYearAndMonth(selection, month, year, "releaseDate");
            }
            return selection.ToList();
        }
        
    }
}