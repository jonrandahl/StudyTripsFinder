﻿using System.Linq;
using System.Text;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace TGDH.Core.ExtensionMethods
{
  public static class ImageExtensionMethods
  {

    public static IHtmlString GetSrcSet(this IPublishedContent item, int width = 0, int height = 0, string widths = "94,300,600,768,1024,1280,1600,1920")
    {
      var sb = new StringBuilder(string.Empty);

      if (width == 0) { width = int.Parse(item.GetProperty("umbracoWidth").Value.ToString()); }
      if (height == 0) { height = int.Parse(item.GetProperty("umbracoHeight").Value.ToString()); }
      var splitWidths = widths.Split(',');

      foreach (var newWidth in splitWidths)
      {
        var newHeight = CalcHeight(width, height, int.Parse(newWidth));
        sb.Append(item.GetCropUrl(width: int.Parse(newWidth), height: newHeight));
        sb.Append(" ");
        sb.Append(newWidth);
        sb.Append("w");
        if (newWidth != splitWidths.Last())
        {
          sb.Append(",");
        }

      }
      return (new HtmlString(sb.ToString()));
    }

    public static int CalcHeight(int paramOldWidth, int paramOldHeight, int paramNewWidth)
    {
      float oldWidth = paramOldWidth;
      var scaleFactor = paramNewWidth / oldWidth;

      var newHeight = paramOldHeight * scaleFactor;
      return ((int)newHeight);
    }

  }
}