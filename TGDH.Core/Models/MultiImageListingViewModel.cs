using System.Collections.Generic;
using Umbraco.Core.Models;

namespace TGDH.Core.Models
{
    public class MultiImageListingViewModel
    {
        public IEnumerable<IPublishedContent> Images { get; set; }

        public RJP.MultiUrlPicker.Models.Link Link { get; set; }

        public string Headline { get; set; }

        public string Copy { get; set; }

        public string ModifierClass { get; set; }
    }
}
