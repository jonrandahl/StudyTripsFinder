using System.Web;
using Umbraco.Core.Models;

namespace TGDH.Core.Models
{
    public class ReviewViewModel
    {
        public int StarRatingCount { get; set; }

        public IHtmlString Quote { get; set; }

        public IPublishedContent Image { get; set; }

        public string Author { get; set; }

        public string Source { get; set; }

        public bool IsCompact { get; set; }

        public string ModifierClass { get; set; }

        public ReviewViewModel()
        {

        }
    }
}