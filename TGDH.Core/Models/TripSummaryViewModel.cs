using Umbraco.Core.Models;

namespace TGDH.Core.Models
{
    public class TripSummaryViewModel
    {
        public IPublishedContent Image { get; set; }

        public string Url { get; set; }

        public string Headline { get; set; }

        public string Subtitle { get; set; }

        public string Label { get; set; }

        public string Copy { get; set; }

        public string Country { get; set; }

        public string Subject { get; set; }

        public bool IsRecommended { get; set; }

        public IPublishedContent Operator { get; set; }

        public string ModifierClass { get; set; }

        public TripSummaryViewModel()
        {

        }
    }

}