
using Umbraco.Core.Models;

public class LogoViewModel
{
    public IPublishedContent Logo { get; set; }

    public string Name { get; set; }

    public string Label { get; set; }

    public RJP.MultiUrlPicker.Models.Link Link { get; set; }

    public int ImageWidth { get; set; }

    public string ModifierClass { get; set; }

    public LogoViewModel()
    {
        ImageWidth = 200;
    }
}