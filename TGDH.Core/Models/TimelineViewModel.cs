using System.Collections.Generic;
using Umbraco.Core.Models;

namespace TGDH.Core.Models
{
    public class TimelineViewModel
    {
        public IEnumerable<IPublishedContent> Points { get; set; }

        public string ModifierClass { get; set; }

        public TimelineViewModel()
        {
        }
    }
}