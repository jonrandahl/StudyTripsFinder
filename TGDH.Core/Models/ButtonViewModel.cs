namespace TGDH.Core.Models
{
    public class ButtonViewModel
    {
        public RJP.MultiUrlPicker.Models.Link Link { get; set; }
        
        public string ModifierClass { get; set; }

        public ButtonViewModel()
        {

        }
    }
}