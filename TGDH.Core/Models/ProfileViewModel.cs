using Umbraco.Core.Models;

namespace TGDH.Core.Models
{
    public class ProfileViewModel
    {
        public IPublishedContent Image { get; set; }

        public string Headline { get; set; }

        public string Subtitle { get; set; }

        public string Email { get; set; }

        public string Tel { get; set; }

        public string ModifierClass { get; set; }

        public ProfileViewModel()
        {
        }
    }
}
