using System.Collections.Generic;
using Umbraco.Core.Models;

namespace TGDH.Core.Models
{
    public class IconListViewModel
    {
        public string[] Bullets { get; set; }

        public string IconRef { get; set; }

        public string ModifierClass { get; set; }

        public IconListViewModel()
        {
        }
    }
}