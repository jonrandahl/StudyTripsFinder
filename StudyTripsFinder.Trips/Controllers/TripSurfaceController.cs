﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudyTripsFinder.Trips.Data;
using Examine;
using Examine.LuceneEngine.SearchCriteria;
using Examine.SearchCriteria;
using Umbraco.Core;
using Umbraco.Web.Mvc;
using Umbraco.Web;
using Umbraco.Core.Models;
using TGDH.Core.Data;

namespace StudyTripsFinder.Trips.Controllers
{
    public class TripSurfaceController : SurfaceController
    {
        public ActionResult GetFilteredTrips(int offset, int size, string keywords = "", string subject = "", string country = "", string city = "")
        {
            var allTrips = GetTripsBySearch(keywords, subject, country, city);
            var queryStringValues = new NameValueCollection();

            if (!string.IsNullOrWhiteSpace(keywords))
            {
                queryStringValues.Add("keywords", keywords);
            }

            if (!string.IsNullOrWhiteSpace(subject))
            {
                queryStringValues.Add("subject", subject);
            }

            if (!string.IsNullOrWhiteSpace(country))
            {
                queryStringValues.Add("country", country);
            }

            if (!string.IsNullOrWhiteSpace(city))
            {
                queryStringValues.Add("city", city);
            }

            if (offset > 0)
            {
                offset = offset - 1;
            }

            var activePage = offset + 1;

            var pagedTrips = allTrips.Skip(offset * size).Take(size);

            return View(pagedTrips);
        }

        public List<IPublishedContent> GetTripsBySearch(string keywords = "", string subject = "", string country = "", string city = "", int limit = 0)
        {
            List<IPublishedContent> allTrips;

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            allTrips = TripsRepository.AllTrips(umbracoHelper).ToList();

            allTrips = FilterTripsBySubject(allTrips, subject);
            allTrips = FilterTripsByLocation(allTrips, country, city);

            if (limit > 0) {
                allTrips = allTrips.Take(limit).ToList();
            }

            return allTrips;
        }

        public List<IPublishedContent> FilterTripsBySubject(List<IPublishedContent> trips, string subject)
        {
            if (string.IsNullOrEmpty(subject)) return trips;

            trips = trips.Where(x => x.GetPropertyValue<string>("subject").ToUrlSegment().InvariantEquals(subject)).ToList();

            return trips;
        }

        public List<IPublishedContent> FilterTripsByCountry(List<IPublishedContent> trips, string country)
        {
            if (string.IsNullOrEmpty(country)) return trips;

            var allTrips = new List<IPublishedContent>();

            foreach (var item in trips) {
                var location = item.GetPropertyValue<IPublishedContent>("location");
                if (location != null) {
                    var countrySlug = "";
                    var docType = location.DocumentTypeAlias;
                    var isCity = docType == "city";

                    if (isCity) {
                        countrySlug = location.Ancestor().Name;
                    } else {
                        countrySlug = location.Name;
                    }

                    countrySlug = countrySlug.ToUrlSegment();

                    if (countrySlug.InvariantEquals(country)) {
                        allTrips.Add(item);
                    }
                }
            }

            return allTrips;
        }

        public List<IPublishedContent> FilterTripsByLocation(List<IPublishedContent> trips, string country, string city)
        {
            if (string.IsNullOrEmpty(country) && string.IsNullOrEmpty(city)) return trips;

            var allTrips = new List<IPublishedContent>();

            foreach (var item in trips) {
                var location = item.GetPropertyValue<IEnumerable<IPublishedContent>>("location").FirstOrDefault();
                if (location != null) {
                    var docType = location.DocumentTypeAlias;
                    var isCountry = docType == "country";
                    var isCity = docType == "city";

                    if (isCountry) {
                        if (location.Name.ToUrlSegment().InvariantEquals(country) && String.IsNullOrWhiteSpace(city)) {
                            allTrips.Add(item);
                        }
                    } else if (isCity) {
                        if (String.IsNullOrWhiteSpace(city)) {
                            if (location.Ancestor().Name.ToUrlSegment().InvariantEquals(country)) {
                                allTrips.Add(item);
                            }
                        } else {
                            if (location.Name.ToUrlSegment().InvariantEquals(city)) {
                                allTrips.Add(item);
                            }
                        }
                        
                    }

                }
            }

            return allTrips;
        }

    }
}