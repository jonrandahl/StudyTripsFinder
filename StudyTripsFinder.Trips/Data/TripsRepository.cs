﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Web;
using Umbraco.Core.Models;

namespace StudyTripsFinder.Trips.Data
{
    public static class TripsRepository
    {
        public static IEnumerable<IPublishedContent> AllTrips(UmbracoHelper umbraco)
        {
            var root = umbraco.TypedContentSingleAtXPath("//trips");
            var selection = root.Descendants("trip").Where(x => x.IsVisible());

            return selection;
        }
    }
}